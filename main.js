// We can use express as shown as below
const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.listen(port, () => {
    console.log(`Mediatek app listening at http://localhost:${port}`)
})